<?php require_once 'data.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $meta_description; ?>">
    <meta name="keywords" content="<?php echo $meta_keywords; ?>">

    <link rel="stylesheet" type="text/css" media="screen" href="assets/css/main.css" />

    <?php if(isset($gtm_id) && $gtm_id != '') { ?>
    <!-- Add GTM Code -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $gtm_id; ?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', '<?php echo $gtm_id; ?>');
    </script>
    
    <?php } ?>

</head>
<body>
    
    <form name="lead-form" id="lead-form" method="post" action="#">
        <div id="form-response"></div>
        <div class="form-group">
            <label>Name: </label>
            <input type="text" name="txt_name">
        </div>
        <div class="form-group">
            <label>Mobile: </label>
            <input type="number" name="txt_mobile">
        </div>
        <div class="form-group">
            <label>Email: </label>
            <input type="email" name="txt_email">
        </div>
        <div class="form-group">
            <label>Website: </label>
            <input type="url" name="txt_website">
        </div>
        <div class="form-group">
            <label>Gender: </label>
            <label><input type="radio" name="rd_gender" value="Male">Male</label>
            <label><input type="radio" name="rd_gender" value="Female">Female</label>
        </div>
        <div class="form-group">
            <label>Skills: </label>
            <label><input type="checkbox" name="chk_skills[]" value="PHP">PHP</label>
            <label><input type="checkbox" name="chk_skills[]" value="jQuery">jQuery</label>
            <label><input type="checkbox" name="chk_skills[]" value="CSS3">CSS3</label>
            <label><input type="checkbox" name="chk_skills[]" value="HTML5">HTML5</label>
            <label><input type="checkbox" name="chk_skills[]" value="Codeigniter">Codeigniter</label>
            <label><input type="checkbox" name="chk_skills[]" value="JAVA">JAVA</label>
        </div>
        <div class="form-group">
            <input type="submit" name="submit" value="Save">
        </div>
    </form>
    
    <script src="//code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.12.0/jquery.validate.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>