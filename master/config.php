<?php
	error_reporting(0);
	if (session_status() == PHP_SESSION_NONE) {
	    session_start();
	}
	
	date_default_timezone_set("Asia/Kolkata");
	ini_set("date.timezone", "Asia/Kolkata");
	
	require_once 'clsDatabase.php';
	
	
	function utf8ize($d) {
		if (is_array($d)) {
			foreach ($d as $k => $v) {
				$d[$k] = utf8ize($v);
			}
		} else if (is_string ($d)) {
			return utf8_encode($d);
		}
		return $d;
	}
	
	if($_SERVER['HTTP_HOST'] == "localhost")
	{
        define(DB_NAME, 'test');
		define(DB_USER, 'root');
		define(DB_PASSWORD, '');
		define(DB_HOST, 'localhost');
	}
	else 
	{
		define(DB_NAME, '');
		define(DB_USER, '');
		define(DB_PASSWORD, '');
		define(DB_HOST, '');
	}
	$databaseObject = new Database(DB_NAME, DB_USER, DB_PASSWORD, DB_HOST);
?>