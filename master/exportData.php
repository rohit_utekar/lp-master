<?php
	require_once 'config.php';

	// DB table to use
	$table = 'tbl_lp_leads';
	 
	// Table's primary key
	$primaryKey = 'id';
	 
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes

	$columnsNames = array(
			"#",
			"Name",
			"Email ID",
			"Mobile",
			"Website",
			"Gender",
			"Skills",
			"Campaign Name",
			"Date Created",
			"Ip Address",
			"User agent",
			"Utm source",
			"Utm medium",
			"Utm campaign",
			"Utm term",
			"Utm content",
		);
	$columns = array(
		array( 'db' => 'name', 'dt' => 1 ),
		array( 'db' => 'email', 'dt' => 2 ),
		array( 'db' => 'mobile', 'dt' => 3 ),
		array( 'db' => 'website', 'dt' => 4 ),
	    array( 'db' => 'gender', 'dt' => 5 ),
	    array( 'db' => 'skills', 'dt' => 6 ),
	    array( 'db' => 'campaign_name', 'dt' => 7 ),
	    array(
	        'db'        => 'date_created',
	        'dt'        => 8,
	        'formatter' => function( $d, $row ) {
	            return date( 'jS M Y H:i:s', strtotime($d));
	        }
		),
		array( 'db' => 'ip_address', 'dt' => 9 ),
		array( 'db' => 'user_agent', 'dt' => 10 ),
		array( 'db' => 'utm_source', 'dt' => 11 ),
		array( 'db' => 'utm_medium', 'dt' => 12 ),
		array( 'db' => 'utm_campaign', 'dt' => 13 ),
		array( 'db' => 'utm_term', 'dt' => 14 ),
		array( 'db' => 'utm_content', 'dt' => 15 )
	);
	 
	// SQL server connection information
	$sql_details = array(
	    'user' => DB_USER,
	    'pass' => DB_PASSWORD,
	    'db'   => DB_NAME,
	    'host' => DB_HOST
	);
	 
	 
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require_once( 'ssp.class.php' );
	
	header("Content-type: application/octet-stream");  
	header("Content-Disposition: attachment; filename=Report.xls");  
	header("Pragma: no-cache");  
	header("Expires: 0");  
	
	$columnHeader = implode("\t", $columnsNames);

	$exportDataArr = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

	$exportData = "";
	if (is_array($exportDataArr['data']) && count($exportDataArr['data']) > 0) {
		$ctr = 1;
		foreach ($exportDataArr['data'] as $record) {
			$rowData = '"' . ($ctr++) . '"' . "\t";  
			foreach ($record as $value) {  
		        $value = '"' . $value . '"' . "\t";  
		        $rowData .= $value;  
		    }  
		    $exportData .= trim($rowData) . "\n";
		}
	}

	echo ucwords($columnHeader) . "\n" . $exportData . "\n"; 
?>