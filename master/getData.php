<?php
	require_once 'config.php';
	 
	// DB table to use
	$table = 'tbl_lp_leads';
	 
	// Table's primary key
	$primaryKey = 'id';
	 
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array( 'db' => 'id', 'dt' => 0 ),
		array( 'db' => 'name', 'dt' => 1 ),
		array( 'db' => 'email', 'dt' => 2 ),
		array( 'db' => 'mobile', 'dt' => 3 ),
		array( 'db' => 'website', 'dt' => 4 ),
	    array( 'db' => 'gender', 'dt' => 5 ),
	    array( 'db' => 'skills', 'dt' => 6 ),
	    array( 'db' => 'campaign_name', 'dt' => 7 ),
	    array(
	        'db'        => 'date_created',
	        'dt'        => 8,
	        'formatter' => function( $d, $row ) {
	            return date( 'jS M Y H:i:s', strtotime($d));
	        }
		)
	);
	 
	// SQL server connection information
	$sql_details = array(
	    'user' => DB_USER,
	    'pass' => DB_PASSWORD,
	    'db'   => DB_NAME,
	    'host' => DB_HOST
	);
	 
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require_once( 'ssp.class.php' );
	$res = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );
	
	echo json_encode(utf8ize($res));
?>