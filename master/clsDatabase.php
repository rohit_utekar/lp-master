<?php 	
	header( 'Content-Type: text/html; charset=utf-8' );
	/**
	* Author: Rohit Utekar
	* Date Created: 16th dec, 2014
	*/
	class Database {
		var $debug = FALSE;  				// ON / OFF debug Mode
		var $lastError = NULL;  			// Holds the last error
		var $lastQuery;         			// Holds the last query
		var $result;           				// Holds the MySQL query result
		var $records;           			// Holds the total number of records returned
		var $affected;          			// Holds the total number of records affected
		var $rawResults;        			// Holds raw 'arrayed' results
		var $arrayedResult;     			// Holds an array of the result
		
		var $hostname;          			// MySQL Hostname
		var $username;          			// MySQL Username
		var $password;          			// Password
		var $database;          			// Database
		var $persistant;          			// Persistant Connection ?
		
		var $connection;      				// Database Connection Link
		var $affectedRowsCount; 			// Database Affected row count
		var $lastInsertID; 					// Database Last Query Insert ID
		var $selectedRowsCount;      		// Database selected row count
		var $timeZone;						// Timezone
		
		private $query_tpl = array();

		function __construct($database, $username, $password, $hostname='localhost', $port=3306, $persistant = FALSE) {
			$this->database = $database;
			$this->username = $username;
			$this->password = $password;
			$this->hostname = $hostname;//.':'.$port;
			$this->persistant = $persistant;
			$this->setTimeZone();
			
			$this->query_tpl['select'] = 'SELECT SQL_CALC_FOUND_ROWS [FIELDS] FROM [TABLE] [JOIN] [CONDITIONS] [COMPARES] [SEARCH] [GROUP] [ORDER] [SORT] [LIMIT]';
			$this->query_tpl['insert'] = 'INSERT INTO [TABLE] [KEY] [VALUE]';
			$this->query_tpl['update'] = 'UPDATE [TABLE] [SET] [CONDITIONS] [COMPARES] [LIMIT]';
			$this->query_tpl['delete'] = 'DELETE FROM [TABLE] [CONDITIONS] [COMPARES] [LIMIT]';
			$this->query_tpl['describe'] = 'DESCRIBE [TABLE]';
			
			$this->connect();
		}
		
		/* Connect to database server */
		public function connect() {
			$this->closeConnection();
			$this->connection = new mysqli($this->hostname, $this->username, $this->password);
			if ($this->connection->connect_error) {
			    $this->lastError = 'Connect Error : (' . $this->connection->connect_errno . ') ' . $this->connection->connect_error;
			    return FALSE;
			}
			$this->useDatabase();
			return true;
		}

		/* Select database */
		public function useDatabase(){
			if (!$this->connection->select_db($this->database)) {
				$this->lastError = 'Could not connect to database : (' . $this->connection->connect_errno . ') ' . $this->connection->connect_error;
				return FALSE;
			} else {
				return true;
			}			
		}
		
		/* Close database connection */
	    public function closeConnection(){
	        if($this->connection){
	            $this->connection->close();	
	        }
	    }
		
		/* Enable debug mode */
		public function enableDebugMode() {
			$this->debug = TRUE;
		}
		
		function cleanInput($input) {
			$cleaned = mysqli_real_escape_string($this->connection, $input);
			return $cleaned;
		}

	    function setTimeZone($time_zone = "Asia/Calcutta") {
	    	$this->timeZone = $time_zone;
	    	date_default_timezone_set($time_zone);
	    }

	    function getTimeZone() {
	    	return $this->timeZone;
	    }

	    function currentDate($date_format = 'Y-m-d H:i:s'){
			return date($date_format);
	    }	

		function execQuery($query) {
			$result= $this->connection->query($query);
			if (!$result) {
	   			$this->lastError = $this->connection->error;
	   			return FALSE;
			} else{		
				return $result;
			}		
		}

		function createTable($query) {
			return $this->execQuery($query);
		}

		function truncateTable($table) {
			return $this->execQuery("TRUNCATE table ".$table);
		}

		function dropTable($table) {
			return $this->execQuery("DROP table ".$table);
		}

	    function getAutoIncrement($table){
	    	$query = "SHOW TABLE STATUS LIKE '".$table."'";		
			$result = $this->execQuery($query);
			
			if (!$result) {
	   			printf("Errormessage: %s\n", $this->connection->error);
			}else{		
				$output = $result->fetch_object();
				$result = $output->Auto_increment;
				return($result);
			}		
	    }

		function createSelectQuery($table = "", $columns = "*", $whereClause = "", $groupBy = "", $orderBy = "", $limit = ""){
			if ($table == "") {
				return "";
			}
			$query = "select $columns from $table";
			if ($whereClause != "") {
				$query .= " where $whereClause ";
			}
			if ($groupBy != "") {
				$query .= " group by $groupBy ";
			}
			if ($orderBy != "") {
				$query .= " order by $orderBy ";
			}
			if ($limit != "") {
				$query .= " limit $limit "; 
			}
			return $query;
		}

		function pushArgument($query, $arguments, $operation) {
			$type = $arguments[0];
			unset($arguments[0]);
			$data = is_array($arguments) ? array_values($arguments) : '';
			$this->connection->query("SET character_set_connection = utf8");
			$this->connection->query("SET character_set_client = utf8");
			$this->connection->query("SET character_set_results = utf8");
			$this->connection->query("SET NAMES utf8");
			
			$response = FALSE;
			switch ($operation) {
				case 'FETCH_DATA':
					$response = $this->fetchData($query, $data, $type);
					break;

				case 'INSERT':
					$response = $this->insertRow($query, $data, $type);
					break;

				case 'UPDATE':
					$response = $this->updateRow($query, $data, $type);
					break;

				case 'DELETE':
					$response = $this->deleteRow($query, $data, $type);
					break;
					
				case 'ALTER':
					$response = $this->alterTable($query, $data, $type);
					break;
				
				default:
					$this->lastError = "Invalid Operation.";
					break;
			}
			
			if($this->debug && $this->lastError){echo $this->lastError;} // If debug mode is enabled show error
			
			return $response;
		}

		function execStmtQuery($query, $data, $type){
			if(is_null($data) || !$data) {
				if ($this->connection->query($query) === TRUE) {
					$this->affectedRowsCount = isset($this->connection->affected_rows) ? $this->connection->affected_rows : 0;
					$this->selectedRowsCount = isset($this->connection->num_rows) ? $this->connection->num_rows : 0;
					$this->lastInsertID = isset($this->connection->insert_id) ? $this->connection->insert_id : 0;
					return TRUE;
				} else {
					$this->affectedRowsCount = 0;
					$this->selectedRowsCount = 0;
					$this->lastInsertID = 0;
					$this->lastError = "Error while executing query: " . $this->connection->error;
					return FALSE;
				}				
			}
			elseif ($stmt = $this->connection->prepare($this->cleanInput($query))) {
				if (is_array($data)) {
					call_user_func_array(array($stmt, 'bind_param'), $this->prepareBindParam($data, $type));
				}
				$stmt->execute();
				$this->lastError = NULL;
				$ret = $stmt;
				$this->affectedRowsCount = $stmt->affected_rows;
				$this->selectedRowsCount = $stmt->num_rows;
				$this->lastInsertID = $stmt->insert_id;
			    $stmt->close();
			    return $ret;
			} else {
				$this->affectedRowsCount = 0;
				$this->selectedRowsCount = 0;
				$this->lastInsertID = 0;
				$this->lastError = "Failed to prepare statement - \"".$query."\"";
				return FALSE;
			}
		}

		function prepareBindParam($data, $type) {
			$preparedParam = array();
			$preparedParam[] = $type;			 
			for($i = 0; $i < strlen($type); $i++) {
			  	$preparedParam[] = & $data[$i];
			}
			return $preparedParam;
		}

		function fetchData($query, $data, $type){
			if ($this->connection->multi_query($query)) {
			  do {
				if ($result = $this->connection->store_result()) {
				  while ($row = $result->fetch_assoc()) 
				  $returns[] = $row;
				  $result->free();
				}
				
			  } while ($this->connection->more_results() && $this->connection->next_result());
			}
			$this->selectedRowsCount = count($returns);
			return ( (isset($returns)) ? $returns : NULL );
		}

		function insertRow($query, $data, $type){
			$result = $this->execStmtQuery($query, $data, $type);
			if ($this->lastError) {
				return FALSE;
			} else {
				return $this->getLastInsertID();
			}
		}

		function updateRow($query, $data, $type){
			$result = $this->execStmtQuery($query, $data, $type);
			if ($this->lastError) {
				return FALSE;
			} else {
				return $this->getAffectedRowsCount();
			}
		}

		function deleteRow($query, $data, $type){
			$result = $this->execStmtQuery($query, $data, $type);
			if ($this->lastError) {
				return FALSE;
			} else {
				return $this->getAffectedRowsCount();
			}
		}

		function alterTable($query, $data, $type){
			$result = $this->execStmtQuery($query, $data, $type);
			if ($this->lastError) {
				return FALSE;
			} else {
				return TRUE;
			}
		}

	    function getLastInsertID(){
	        return $this->lastInsertID;
	    }

	    function getAffectedRowsCount(){
	        return $this->affectedRowsCount;
	    }

	    function getSelectedRowsCount(){
	        return $this->selectedRowsCount;
	    }	
	}
	
	/*
	*	// Connection
	*	$databaseObject = new Database(DB_NAME, USERNAME, PASSWORD, HOST_NAME);
	*	$databaseObject->enableDebugMode();
	*
	
	* 	// Types
	*	s -> corresponding variable has type string
	*	i -> corresponding variable has type integer
	*	d -> corresponding variable has type double
	*	b -> corresponding variable is a blob and will be sent in packets
	
	*	// Insert Row
	*	$sp_insert = "insert into test(title) values(?)";
	*	$rs_insert = $databaseObject->pushArgument($sp_insert, array('s', 'Test'), 'INSERT');
	*


	*	// Delete Row
	*	$sp_update = "update test set title = ? where id = ?";
	*	$rs_update = $databaseObject->pushArgument($sp_update, array('si', 'test 222', 1), 'UPDATE');
	
	
	*	// Delete Row
	*	$sp_delete = "delete from test where id = ?";
	* 	$rs_delete = $databaseObject->pushArgument($sp_delete, array('i', 73), 'DELETE');
	
	
	*	// Select Rows
	*	$sp_select = "select id, title from test";
	*	$rs_select = $databaseObject->pushArgument($sp_select, 0, 'FETCH_DATA');
	
	
	*	// Alter Table
	*	$sp_alter = "ALTER TABLE `test` CHANGE `name` `title` VARCHAR(500) NOT NULL;";
	*	$rs_alter = $databaseObject->pushArgument($sp_alter, null, 'ALTER');
	
	
	*	// Drop Table
	*	$sp_drop = "DROP TABLE `test`;";
	*	$rs_drop = $databaseObject->pushArgument($sp_drop, null, 'ALTER');
	
	
	*	// Create Table
	*	$sp_create = "CREATE TABLE IF NOT EXISTS `test` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `title` varchar(500) NOT NULL,
				  PRIMARY KEY (`id`)
				)";
	*	$rs_create = $databaseObject->pushArgument($sp_create, null, 'ALTER');
	
	
	*/
	
?>