<?php
	require_once("config.php");
	session_start();

	if (isset($_SESSION['login_user'])) {
		header("location: dashboard.php");
	}

	if($_POST['submit'] == "Sign me in"){
		$username = addslashes($_POST['username']); 
		$password = md5(addslashes($_POST['password'])); 

		$sp_login = "SELECT id, auth_key, passcode FROM tbl_lp_users WHERE username = '".$username."' AND is_active = 1";
		$rs_login = $databaseObject->pushArgument($sp_login, array(), 'FETCH_DATA');
		
		$user_id = $rs_login[0]['id'];
		$auth_key = $rs_login[0]['auth_key'];
		$passcode = $rs_login[0]['passcode'];

		$count = count($rs_login);
		if($count == 1 && $passcode == $password)
		{
			$_SESSION['login_user'] = $username;
			$_SESSION['my_user_id'] = $user_id;
			$_SESSION['my_auth_key'] = $auth_key;
			$enter_date = $databaseObject->currentDate();
			header("location: dashboard.php");
		} else {
			$error = "The username or password you entered is incorrect.";
			$has_error = "has-error";
		}
	} 
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Admin Login</title>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="assets/css/custom.css"/>  
	</head>
	<body class='login'>
		<div class="wrapper">
			<h1>
				<a class="logo" href="javascript:;">Admin</a>
			</h1>
			<div class="login-body">
				<h2>SIGN IN</h2>
				<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method='POST' class='form-validate' id="login-form">
					<div class="form-group">
						<div class="email controls">
							<input type="text" name='username' placeholder="Email address" value="<?php echo $username; ?>" class='form-control' data-rule-required="true" data-rule-email="true" <?php if($error == ''){ echo "autofocus"; } ?>>
						</div>
					</div>
					<div class="form-group <?php echo $has_error; ?>">
						<div class="pw controls">
							<input type="password" name="password" placeholder="Password" class='form-control' data-rule-required="true" <?php if($error != ''){ echo "autofocus"; } ?>>
							<span for="login" class="help-block has-error"><?php echo $error; ?></span>
						</div>
					</div>
					<div class="submit">
						<input name="submit" type="submit" value="Sign me in" class='btn btn-success'>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>
