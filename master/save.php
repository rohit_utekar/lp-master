<?php
	session_start();
	require_once 'config.php';
	
	$campaign_name = 'Test';

	$name = $_REQUEST['txt_name'];
	$email = $_REQUEST['txt_email'];
	$mobile = $_REQUEST['txt_mobile'];
	$website = $_REQUEST['txt_website'];
	$gender = $_REQUEST['rd_gender'];
	$skills = implode(",", $_REQUEST['chk_skills']);

	$utm_source = $_REQUEST['utm_source'];
	$utm_medium = $_REQUEST['utm_medium'];
	$utm_campaign = $_REQUEST['utm_campaign'];
	$utm_term = $_REQUEST['utm_term'];
	$utm_content = $_REQUEST['utm_content'];
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	$ip_address = $_SERVER['REMOTE_ADDR'];
	$date_created =	$databaseObject->currentDate();

	$sp_insert = "insert into tbl_lp_leads(`name`, `email`,`mobile`, `website`, `gender`, `skills`, `campaign_name`, `user_agent`, `utm_source`, `utm_medium`, `utm_campaign`, `utm_term`, `utm_content`, `ip_address`, `date_created`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	$rs_insert = $databaseObject->pushArgument($sp_insert, array('sssssssssssssss', $name, $email, $mobile, $website, $gender, $skills, $campaign_name, $user_agent, $utm_source, $utm_medium, $utm_campaign, $utm_term, $utm_content, $ip_address, $date_created), 'INSERT');

	if($rs_insert > 0) {	
		$_SESSION["save"] = "true";	
		echo json_encode(array('status' => 'success', 'msg' => '<div class="success">Thank you for sharing your details.  We will get back to you shortly.</div>'));
	} 
	else {
		echo json_encode(array('status' => 'failure', 'msg' => '<div class="error">Error while processing data.</div>'));
	}
?>