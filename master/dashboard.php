<?php
	include_once 'header.php';

    $columns = array(
        "#",
        "Name",
        "Email ID",
        "Mobile",
        "Website",
        "Gender",
        "Skills",
        "Campaign Name",
        "Date Created",
    );

    $dataURL = "getData.php";
    $exportURL = "exportData.php";
?>
	<h1 class="mt-5">Leads</h1>
	<table class="table table-bordered table-striped" id="example" cellspacing="0" width="100%">
        <thead>
            <tr>
            <?php
                for ( $i = 0 ; $i < count($columns) ; $i++ ) {
                    echo '<th>'.$columns[$i].'</th>';
                }
            ?>
            </tr>
        </thead>
    </table>
<?php
	include_once 'footer.php';
?>
<script type="text/javascript">
	$(function() {
	    var table = $('#example').DataTable( {
	        "processing": true,
	        "serverSide": true,
	        "ajax": "<?php  echo $dataURL; ?>",
            "initComplete": function(settings, json) {
                new $.fn.dataTable.Buttons(table, {
                    buttons: [
                        {
                            text: 'Export All to Excel',
                            className: "btn-success btn-sm ml-md-3",
                            action: function (e, dt, button, config)
                            {
                                window.open('<?php echo $exportURL; ?>')
                            }
                        }
                    ]
                });    
                var btns = table.buttons();
                var btncontainer = table.buttons().container();
                btncontainer.appendTo('#example_wrapper #example_length');
            }
	    });
	}); 
</script>