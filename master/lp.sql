-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 25, 2018 at 06:20 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lp_leads`
--

CREATE TABLE `tbl_lp_leads` (
  `id` int(100) NOT NULL,
  `campaign_name` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `website` varchar(1000) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `skills` varchar(500) DEFAULT NULL,
  `utm_source` varchar(50) DEFAULT NULL,
  `utm_medium` varchar(50) DEFAULT NULL,
  `utm_campaign` varchar(50) DEFAULT NULL,
  `utm_term` varchar(50) DEFAULT NULL,
  `utm_content` varchar(50) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `user_agent` varchar(100) DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `tbl_lp_users`
--

CREATE TABLE `tbl_lp_users` (
  `id` int(11) NOT NULL,
  `username` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `auth_key` varchar(500) DEFAULT NULL,
  `passcode` varchar(500) DEFAULT NULL,
  `is_active` smallint(1) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_lp_users`
--

INSERT INTO `tbl_lp_users` (`id`, `username`, `email`, `auth_key`, `passcode`, `is_active`, `date_created`) VALUES
(1, 'admin', 'admin@gmail.com', 'b24331b1a138cde62aa1f679164fc62f', 'b24331b1a138cde62aa1f679164fc62f', 1, '2017-08-19 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_lp_leads`
--
ALTER TABLE `tbl_lp_leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_lp_users`
--
ALTER TABLE `tbl_lp_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_lp_leads`
--
ALTER TABLE `tbl_lp_leads`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `tbl_lp_users`
--
ALTER TABLE `tbl_lp_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
