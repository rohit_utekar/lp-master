<?php 
    session_start();
    require_once 'data.php'; 
    
    if(!isset($_SESSION["save"])){
        header('location:index.php');
        exit;
    }else{
        unset($_SESSION['save']);
    }      
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $meta_description; ?>">
    <meta name="keywords" content="<?php echo $meta_keywords; ?>">

    <link rel="stylesheet" type="text/css" media="screen" href="assets/css/main.css" />

    <?php if(isset($gtm_id) && $gtm_id != '') { ?>
    <!-- Add GTM Code -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $gtm_id; ?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', '<?php echo $gtm_id; ?>');
    </script>
    
    <?php } ?>
</head>
<body>
    
    <div class="thank-you-container">
        Thank you for sharing your details.  We will get back to you shortly.
    </div>
    
    <script src="//code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.12.0/jquery.validate.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>