$(function() {
  // Custom validation rule for Person name
  $.validator.methods.person_name = function(value, element) {
    return this.optional(element) || /^[a-zA-Z `']+$/.test(value);
  };

  // Custom validation rule for Email
  $.validator.methods.email = function(value, element) {
    return (
      this.optional(element) ||
      /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(
        value
      )
    );
  };

  $("#lead-form").validate({
    rules: {
      txt_name: {
        required: true,
        person_name: true
      },
      txt_email: {
        required: true,
        email: true
      },
      txt_mobile: {
        required: true,
        digits: true,
        minlength: 10,
        maxlength: 10
      },
      txt_website: {
        required: true,
        url: true
      },
      rd_gender: {
        required: true
      },
      "chk_skills[]": {
        required: true
      }
    },
    // Specify validation error messages
    messages: {
      txt_name: {
        required: "Please enter name.",
        person_name: "Please enter valid name"
      },
      txt_email: {
        required: "Please enter email ID.",
        email: "Please enter valid email ID."
      },
      txt_mobile: {
        required: "Please enter mobile number.",
        digits: "Please enter valid mobile number.",
        minlength: "Mobile number should be 10 digits only.",
        maxlength: "Mobile number should be 10 digits only."
      },
      txt_website: {
        required: "Please enter website.",
        url: "Please enter valid website."
      },
      rd_gender: {
        required: "Please select gender."
      },
      "chk_skills[]": {
        required: "Please select skills."
      }
    },
    errorPlacement: function(label, ele) {
      $(ele)
        .closest(".form-group")
        .append(label[0].outerHTML);
      return false;
    },
    submitHandler: function(frm) {
      var form_data = $("#lead-form").serializeArray();

      // Add additional form data in the array
      form_data.push(
        { name: "utm_source", value: getParameterByName("utm_source") },
        { name: "utm_medium", value: getParameterByName("utm_medium") },
        { name: "utm_campaign", value: getParameterByName("utm_campaign") },
        { name: "utm_term", value: getParameterByName("utm_term") },
        { name: "utm_content", value: getParameterByName("utm_content") }
      );

      $.ajax({
        url: "./master/save.php",
        method: "post",
        data: form_data,
        beforeSend: function() {
          $(frm)
            .find("button")
            .text("Processing...");
          $(frm)
            .find("button, input, select")
            .prop("disabled", "disabled");
        },
        success: function(res) {
          $(frm)
            .find("button")
            .text("Submit");
          $(frm)
            .find("button, input, select")
            .prop("disabled", "");
          try {
            var j_res = JSON.parse(res);
            if (j_res.status == "success") {
              window.location.href = "thank-you.php";
            } else {
              $("#form-response")
                .html(j_res.msg)
                .show();
            }
          } catch (e) {}
        },
        error: function() {
          $(frm)
            .find("button")
            .val("Submit");
          $(frm)
            .find("button, input, select")
            .prop("disabled", "");
        }
      });
    }
  });
});

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return "";
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}
